#include "Arduino.h"
#include "Int-eeprom-tf.h"
#include <EEPROM.h>

Int2EEPROM::Int2EEPROM(void) {
}

void Int2EEPROM::write(uint16_t eepromByte, uint16_t value) {
    _eepromByte = eepromByte;
    _value = value;
    _firstByte = int(_value / 255);
    _secondByte = _value - _firstByte * 255;
    EEPROM.write(_eepromByte, _firstByte);
    EEPROM.write(_eepromByte + 1, _secondByte);
}

uint16_t Int2EEPROM::read(uint16_t eepromByte) {
  _eepromByte = eepromByte;
  return (EEPROM.read(_eepromByte) * 255 +
	  EEPROM.read(_eepromByte + 1));
}
