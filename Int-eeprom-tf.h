/*
  Library for saving Unsigned Int to 2 bytes of Arduino's EEPROM
  by Richard Fabo

  
  
*/

#ifndef INT-EEPROM-TF_H_INCLUDED
#define INT-EEPROM-TF_H_INCLUDED

class Int2EEPROM
{
public:
  Int2EEPROM(void);
  void write(uint16_t eepromByte, uint16_t value);
  uint16_t read(uint16_t eepromByte);
  
private:
  uint16_t _eepromByte;
  uint16_t _value;
  uint8_t _firstByte;
  uint8_t _secondByte;
};

#endif // INT-EEPROM-TF_H_INCLUDED
