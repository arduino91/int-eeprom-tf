/* Ukladanie Unsigned Int do EEPROM
   Testovanie
 */

#include <Int-eeprom-tf.h>
#include <EEPROM.h>

Int2EEPROM int2EEPROM;

void setup() {
  Serial.begin(4800);
  Serial.println("*****");
}

void loop() {
  Serial.print("Zadaj uint (0 - 65279): ");

  while (Serial.available() == 0) { }

  float vstup = Serial.parseInt();

  Serial.println("Zapisujem na byte 100 a 101");
  int2EEPROM.write(100, vstup);

  Serial.print("Čítam byte 100: ");
  Serial.println(EEPROM.read(100));
  Serial.print("Čítam byte 101: ");
  Serial.println(EEPROM.read(101));

  Serial.print("Čítam z knižnice: ");
  Serial.println(int2EEPROM.read(100));

  
  
  delay(2000);

  
}